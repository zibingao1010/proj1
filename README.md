# Individual Project 1: Continuous Delivery of Personal Website
Link to my website: https://proj1-zibingao1010-2f5704d2c59dca2f063289ce59d2279213c910f931e1.gitlab.io

## Implementation
1. **Installation:** Visit  [official Zola documentation](https://www.getzola.org/documentation/getting-started/installation/) for detailed installation instructions for your operating system and install Zola on your laptop.
2. **Creating a New Site:** Once Zola is installed, you can create a new site by running the following command in your terminal:

    ```shell
    zola init my_site
    ```

    Replace `my_site` with the name of your project. This command creates a new directory with some basic configuration files and folders.
3. **Using themes:** There are different themes that you could use to build your Zola website. Go to [Themes in Zola](https://www.getzola.org/themes/), and type the following command in your own terminal:
    ```shell
    cd themes
    git clone < theme repository URL >
    ``` 
    Replace the URL with whichever theme you like. 

4. **Adding Content:** Begin adding contents to build up your webpage. For instance, I have a home page with my name and email listed. And there are more information in the `About` and `Projects` pages. I made these modification in the `content` directory.
5. **Test and Build:** With Zola, you can first test your website by running `zola serve`. A url will appear in the terminal and you can get a preview of how your wesite looks locally. Then, you can build your site by running `zola build`. This command generates a public directory with your site's static files, ready to be deployed to a web server.

## Deployment to GitLab Pages
1. **Create a `.gitlab-ci.yml` File:**
   Create a `.gitlab-ci.yml` file in the root of your repository. This file is used by GitLab CI/CD to manage your project's jobs.

2. **Configure the CI/CD Pipeline:**
   Add the following script to your `.gitlab-ci.yml` file:

   ```yaml
    image: alpine:latest

    pages:
        script:
            - apk add --update zola
            - zola build
        artifacts:
            paths:
                - public
        only:
        - master
    ```

3. **Push to GitLab:**
   Commit and push this file to your GitLab repository.

4. **Check Pipeline Status:**
   After pushing, navigate to `CI/CD > Pipelines` in your GitLab repository to check the status of your pipeline. Once it's passed, your site is live.

### Hosted on Netlify
1. Create a file named `netlify.toml` in the root directory of the project.
2. Add deploy commands in `netlify.toml` and push the project onto the GitLab repo. The content of my deployment file:
    ```toml
    [build]
    # This assumes that the Zola site is in a docs folder. If it isn't, you don't need
    # to have a `base` variable but you do need the `publish` and `command` variables.
    publish = "/public"
    command = "zola build"

    [build.environment]
    # Set the version name that you want to use and Netlify will automatically use it.
    ZOLA_VERSION = "0.18.0"

    [context.deploy-preview]
    command = "zola build --base-url $DEPLOY_PRIME_URL"
    ```
3. Sign in to [Netlify](https://app.netlify.com/) and add new sites in **Sites**. Import the repository in the GitLab, the configuration will be filled automatically from the `netlify.toml`.

## Screenshoots
* Website Functionality
![image](website1.png)
![image](website2.png)

* GitLab Workflow
![image](Gitlab.png)

* Hosting and Deployment
![image](Deployment.png)

* Netlify
![image](Netlify1.png)
![image](Netlify2.png)

* Demo Video
https://gitlab.com/zibingao1010/proj1/-/blob/main/Video.mov?ref_type=heads