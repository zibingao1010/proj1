+++
title = "About"
template = "about.html"

[extra]
author = "Zibin Gao"
image = "ID.jpg"
+++

Zibin Gao is a graduate student at Duke University pursuing a Master's in Computer Science, expected to graduate in May 2025. She completed her Bachelor's in Computer Engineering with a minor in Cognitive Science from the University of California, San Diego, achieving a GPA of 3.81/4.0. Amy has diverse experience, including a software development internship at Amazon and academic tutoring at UC San Diego. She has worked on projects like a Book Review Recommender System and an Android Application named ZooSeeker. Her skills encompass programming languages like Java, Python, C/C++, MATLAB, and tools such as AWS, Git, and GitHub.