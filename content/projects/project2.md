+++
title = "Android Application: ZooSeeker"

[extra]
link = ""
technologies = ["group work", "android app", "design principles"]
+++
• Worked in a six-person team to implement visiting direction given the animals a visitor want to see using Agile software process & Behavior-Driven Development

• Used Java to direct visitors to desired destinations with shortest distance based on the map of San Diego zoo

• Created a user-friendly interface for visitors that connected frontend to backend
